package transfer

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

type stringOpener string

func (o stringOpener) Open() (io.ReadCloser, error) {
	return ioutil.NopCloser(bytes.NewBufferString(string(o))), nil
}

var _ Opener = stringOpener("test")

func TestCopyMany(t *testing.T) {
	tempDir := t.TempDir()
	dst := []Creator{
		FileCreator(filepath.Join(tempDir, "dir1/file1.js")),
		FileCreator(filepath.Join(tempDir, "dir1/file2.js")),
		FileCreator(filepath.Join(tempDir, "dir1/file3.js")),
		FileCreator(filepath.Join(tempDir, "dir2/file1.js")),
	}
	src := []Opener{
		stringOpener("dir1 file1 content"),
		stringOpener("dir1 file2 content"),
		stringOpener("dir1 file3 content"),
		stringOpener("dir2 file1 content"),
	}

	err := CopyToMany(dst, src)
	if err != nil {
		t.Fatalf("error calling CopyToFiles: %s", err)
	}
}

func TestCopyManyDir(t *testing.T) {
	tempDir := t.TempDir()
	dst := []Creator{
		FileCreator("dir1/file1.js"),
		FileCreator("dir1/file2.js"),
		FileCreator("dir1/file3.js"),
		FileCreator("dir2/file1.js"),
	}
	src := []Opener{
		stringOpener("dir1 file1 content"),
		stringOpener("dir1 file2 content"),
		stringOpener("dir1 file3 content"),
		stringOpener("dir2 file1 content"),
	}

	err := CopyToManyDir(tempDir, dst, src)
	if err != nil {
		t.Fatalf("error calling CopyToFiles: %s", err)
	}
}

func TestCopyManyBadLength(t *testing.T) {
	tempDir := t.TempDir()
	dst := []Creator{
		FileCreator(filepath.Join(tempDir, "dir1/file2.js")),
		FileCreator(filepath.Join(tempDir, "dir1/file3.js")),
		FileCreator(filepath.Join(tempDir, "dir2/file1.js")),
	}
	src := []Opener{
		stringOpener("dir1 file1 content"),
		stringOpener("dir1 file2 content"),
		stringOpener("dir1 file3 content"),
		stringOpener("dir2 file1 content"),
	}

	err := CopyToMany(dst, src)
	if err == nil {
		t.Fatalf("expected non nil error, got nil")
	}
	if err != ErrLengthsNotMatching {
		t.Fatalf("exptected err: %s, got err: %s", ErrLengthsNotMatching, err)
	}
}

func TestCreateAll(t *testing.T) {
	const gaeTestDir = "gaesrc_testdir"
	testDir := filepath.Join(t.TempDir(), gaeTestDir)
	testFile := filepath.Join(testDir, "relative/file.js")
	f, err := CreateAll(testFile)
	if err != nil {
		t.Fatalf("error calling CreateAll: %s", err)
	}
	// Test if the file exists.
	if _, err := os.Stat(testFile); err != nil {
		t.Fatalf("file not created")
	}
	t.Logf("file created: %s", f.Name())
	f.Close()
	t.Logf("%#v", f)
}
