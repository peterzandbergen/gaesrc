package transfer

import (
	"errors"
	"io"
	"os"
	"path/filepath"
)

type Opener interface {
	Open() (io.ReadCloser, error)
}

type Creator interface {
	Create() (io.WriteCloser, error)
}

type FileCreator string

func (f FileCreator) Create() (io.WriteCloser, error) {
	return CreateAll(string(f))
}

// temp error for testing.
var errNotImplementedYet = errors.New("not implemented yet")

var (
	ErrLengthsNotMatching = errors.New("length of src and dst do not match")
	ErrTypeError          = errors.New("bad type")
)

// CreateAll creates the given file and the path if necessary.
// It returns an error if the file already exists or if the file cannot be created.
func CreateAll(name string) (*os.File, error) {
	// Create dir if it not exists.
	dir := filepath.Dir(name)
	if _, err := os.Stat(dir); err != nil {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return nil, err
		}
	}
	// Create the file.
	f, err := os.Create(name)
	if err != nil {
		return nil, err
	}
	return f, nil
}

func CopyToFile(dst string, src Opener) error {
	// Open source.
	r, err := src.Open()
	if err != nil {
		return err
	}
	defer r.Close()
	// Create destination.
	w, err := CreateAll(dst)
	if err != nil {
		return err
	}
	defer w.Close()
	_, err = io.Copy(w, r)
	if err != nil {
		return err
	}
	return nil
}

func CopyToOne(dst Creator, src Opener) error {
	// Open source.
	r, err := src.Open()
	if err != nil {
		return err
	}
	defer r.Close()
	// Create destination.
	w, err := dst.Create()
	if err != nil {
		return err
	}
	defer w.Close()
	_, err = io.Copy(w, r)
	if err != nil {
		return err
	}
	return nil
}

func prefixDirFileCreator(dir string, c Creator) (FileCreator, error) {
	fc, ok := c.(FileCreator)
	if !ok {
		return FileCreator(""), ErrTypeError
	}
	return FileCreator(filepath.Join(dir, string(fc))), nil
}

func CopyToManyDir(dir string, dst []Creator, src []Opener) error {
	res := make([]Creator, len(dst))
	for i:=0;  i < len(dst); i++ {
		var err error
		if res[i], err = prefixDirFileCreator(dir, dst[i]); err != nil {
			return err
		}
	}
	return CopyToMany(res, src)
}

func CopyToMany(dst []Creator, src []Opener) error {
	if len(src) != len(dst) {
		return ErrLengthsNotMatching
	}
	for i := 0; i < len(dst); i++ {
		err := CopyToOne(dst[i], src[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func prefixDir(dir string, names []string) []string {
	res := make([]string, len(names))
	for i, name := range names {
		res[i] = filepath.Join(dir, name)
	}
	return res
}
