package command

type Commands map[string]func() error

func NewCommands() Commands {
	return Commands(make(map[string]func() error))
}

func (c Commands) RegisterCommand(name string, cmd func() error) {
	c[name] = cmd
}

func (c Commands) Execute(name string) func() error {
	return c[name]
}
