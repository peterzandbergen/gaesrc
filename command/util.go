package command

import (
	"gaesrc/apps"
)

func getApp(credsFile string, appID string) (*apps.Application, error) {
	c, err := apps.New(credsFile)
	if err != nil {
		return nil, err
	}
	app, err := apps.NewApp(c, appID)
	if err != nil {
		return nil, err
	}
	return app, nil
}

