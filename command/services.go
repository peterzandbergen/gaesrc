package command

import (
	"gaesrc/apps"
)

func printService(s *apps.Service) {

}

func ServicesList(cfg *Config) error {
	app, err := getApp(cfg.CredsFile, cfg.ApplicationID)
	if err != nil {
		return err
	}
	services, err := app.Services()
	if err != nil {
		return err
	}
	for _, s := range services {
		printService(s)
	}
	return nil
}
