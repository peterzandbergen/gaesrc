package command

import "testing"

func TestNewCommands(t *testing.T) {
	cmds := NewCommands()
	if cmds == nil {
		t.Fatalf("failed to create commands")
	}
	nop := func() error {
		return nil
	}
	cmds.RegisterCommand("bla", nop)
	cmds.Execute("bla")()
}
