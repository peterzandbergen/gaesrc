package command

type Config struct {
	CredsFile     string
	TargetDir     string
	ApplicationID string
}
