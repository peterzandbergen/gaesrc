module gaesrc

go 1.15

require (
	cloud.google.com/go/storage v1.11.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.30.0
)
