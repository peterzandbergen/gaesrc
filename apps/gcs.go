package apps

import (
	"context"
	"net/http"

	"google.golang.org/api/appengine/v1"
	"google.golang.org/api/option"
)

type AppEngineClient struct {
	hc      *http.Client
	apiSvc  *appengine.APIService
	appsSvc *appengine.AppsService
}

func (a *AppEngineClient) GetFiles() map[string][]byte {
	return nil
}

func New(credsFile string) (*AppEngineClient, error) {
	aec := &AppEngineClient{
		hc: &http.Client{},
	}
	ctx := context.Background()
	apiSvc, err := appengine.NewService(ctx, option.WithCredentialsFile(credsFile))
	if err != nil {
		return nil, err
	}
	aec.apiSvc = apiSvc
	aec.appsSvc = appengine.NewAppsService(aec.apiSvc)
	return aec, nil
}

func (c *AppEngineClient) GetApp(appsId string) (*appengine.Application, error) {
	getCall := c.appsSvc.Get(appsId)
	res, err := getCall.Do()
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (c *AppEngineClient) ListServices(appsId string) ([]*appengine.Service, error) {
	getCall := c.appsSvc.Services.List(appsId)
	res, err := getCall.Do()
	if err != nil {
		return nil, err
	}
	return res.Services, nil
}

func (c *AppEngineClient) GetService(appsId, servicesId string) (*appengine.Service, error) {
	getCall := c.appsSvc.Services.Get(appsId, servicesId)
	res, err := getCall.Do()
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (c *AppEngineClient) GetVersion(appsId, servicesId, versionsId string) (*appengine.Version, error) {
	getCall := c.appsSvc.Services.Versions.Get(appsId, servicesId, versionsId)
	version, err := getCall.Do()
	if err != nil {
		return nil, err
	}
	return version, nil
}

func (c *AppEngineClient) ListVersions(appsId string, servicesId string) ([]*appengine.Version, error) {
	getCall := c.appsSvc.Services.Versions.List(appsId, servicesId)
	getCall.View("FULL")
	resp, err := getCall.Do()
	if err != nil {
		return nil, err
	}
	return resp.Versions, nil
}

// func (c *AppEngineClient) GetServices() (*appengine.Service, error) {
// 	getCall := c.appsSvc.Services.Get()
// }

// Version.Deployment
// func (c *AppEngineClient) Deployments() ([]appengine.Deployment], error) {
// 	return nil, nil
// }
