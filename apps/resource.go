package apps

import (
	"context"

	"google.golang.org/api/cloudresourcemanager/v1"
	"google.golang.org/api/option"
)

type ResourceManager struct {
	svc *cloudresourcemanager.Service
}

func NewResourceManager(credsFile string, options ...option.ClientOption) (*ResourceManager, error) {
	rm, err := cloudresourcemanager.NewService(context.Background(), options...)
	if err != nil {
		return nil, err
	}
	return &ResourceManager{
		svc: rm,
	}, nil
}

func (r *ResourceManager) ListProjects() ([]*cloudresourcemanager.Project, error) {
	listCall := r.svc.Projects.List()
	resp, err := listCall.Do()
	if err != nil {
		return nil, err
	}
	return resp.Projects, nil

}

