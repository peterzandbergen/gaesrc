package apps

import (
	"net/url"
	"strings"

	"google.golang.org/api/appengine/v1"
)

type Ref struct {
	key string
}

type Application struct {
	aec         *AppEngineClient
	application *appengine.Application
}

func NewApp(aec *AppEngineClient, appId string) (*Application, error) {
	app := &Application{
		aec: aec,
	}
	var err error
	app.application, err = aec.GetApp(appId)
	if err != nil {
		return nil, err
	}
	return app, nil
}

func (a *Application) Value() *appengine.Application {
	return a.application
}

func (a *Application) Services() ([]*Service, error) {
	svc, err := a.aec.ListServices(a.application.Id)
	if err != nil {
		return nil, err
	}
	res := make([]*Service, 0, len(svc))
	for _, s := range svc {
		res = append(res, &Service{
			app:     a,
			service: s,
		})
	}
	return res, nil
}

type Service struct {
	app     *Application
	service *appengine.Service
}

func (a *Service) Value() *appengine.Service {
	return a.service
}

func (s *Service) Versions() ([]*Version, error) {
	vs, err := s.app.aec.ListVersions(s.app.application.Id, s.service.Id)
	if err != nil {
		return nil, err
	}
	res := make([]*Version, 0, len(vs))
	for _, v := range vs {
		res = append(res, &Version{
			svc:     s,
			version: v,
		})
	}
	return res, nil
}

type Version struct {
	svc     *Service
	version *appengine.Version
}

func (v *Version) Value() *appengine.Version {
	return v.version
}

func (v *Version) Deployments() ([]*Deployment, error) {
	return nil, nil
}

type Deployment struct {
	v          *Version
	deployment *appengine.Deployment
}

type GcsInfo struct {
	Bucket string
	Object string
}

func GcsInfoFromUrl(name string) (GcsInfo, error) {
	var res GcsInfo
	url, err := url.Parse(name)
	if err != nil {
		return res, err
	}
	parts := strings.Split(url.Path, "/")
	res.Bucket = parts[1]
	res.Object = strings.Join(parts[2:], "/")
	return res, nil
}
