package apps

import (
	"testing"
)

const credsFile = "/home/peza/Secrets/zandbergenmediation-nl-71be644dc54d.json"
const appId = "zandbergenmediation-nl"

// Test getting the deployments.
//
func TestNewClient(t *testing.T) {
	c, err := New(credsFile)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	app, err := c.GetApp(appId)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("app.Name: %s", app.Name)
}

func TestListServices(t *testing.T) {
	c, err := New(credsFile)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	svc, err := c.ListServices(appId)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("len svc: %d", len(svc))
}

func TestGetService(t *testing.T) {
	c, err := New(credsFile)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	svc, err := c.GetService(appId, "default")
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("svc.Name: %s", svc.Name)
}

func TestListVersions(t *testing.T) {
	c, err := New(credsFile)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	vers, err := c.ListVersions(appId, "default")
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("len versions: %d", len(vers))

	for k, f := range vers[0].Deployment.Files {
		t.Logf("%s: %s", k, f.SourceUrl)
	}
}
