package apps

import (
	"context"
	"testing"

	"google.golang.org/api/option"

	// "golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func TestListProjects(t *testing.T) {
	// config, err := google.NewSDKConfig("")
	// if err != nil {
	// 	t.Fatalf("error getting the SDK config: %s", err)
	// }
	ctx := context.Background()
	creds, err := google.FindDefaultCredentials(ctx)
	if err != nil {
		t.Fatalf("error getting the SDK config: %s", err)
	}
	rm, err := NewResourceManager(credsFile, option.WithTokenSource(creds.TokenSource))
	if err != nil {
		t.Fatalf("error creating resource manager: %s", err)
	}
	ps, err := rm.ListProjects()
	if err != nil {
		t.Fatalf("error listing projects: %s", err)
	}
	for i, p := range ps {
		t.Logf("project %2d: %s", i, p.Name)
	}
}
