package apps

import (
	"context"
	"io"
	"testing"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func TestModel(t *testing.T) {
	c, err := New(credsFile)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	app, err := NewApp(c, appId)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("App name: %s", app.application.Name)

	// Get Services.
	svc, err := app.Services()
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("Number of services: %d", len(svc))

	// Get versions.
	vs, err := svc[0].Versions()
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("Number of versions: %d", len(vs))
	// List the files in the deployment.
	var testUri string
	for name, fi := range vs[0].version.Deployment.Files {
		if len(testUri) == 0 {
			testUri = fi.SourceUrl
		}
		t.Logf("name: %s\nsource url: %s\nvalue: %#v", name, fi.SourceUrl, fi)
	}
	// Try to download a gcs resource.
	sc, err := storage.NewClient(context.Background(), option.WithCredentialsFile(credsFile))
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	_ = sc
	defer sc.Close()
	res, err := GcsInfoFromUrl(testUri)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("%#v", res)
	r, err := sc.Bucket(res.Bucket).Object(res.Object).NewReader(context.Background())
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	defer r.Close()
	var nw NullWriter
	n, err := io.Copy(&nw, r)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	t.Logf("written: %d", n)
	defer r.Close()
}

type NullWriter struct{}

func (n *NullWriter) Write(p []byte) (int, error) {
	return len(p), nil
}

func TestGcsInfoFromUrl(t *testing.T) {
	cases := struct {
		uri    string
		bucket string
		object string
	}{
		uri:    "",
		bucket: "",
		object: "",
	}
	_ = cases
}
