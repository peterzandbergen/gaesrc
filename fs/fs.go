package fs

import (
	"errors"
	"os"
)

// FS represents a file system, in our case read an write capable.
type FS interface {
	Open(name string) (File, error)
}

var (
	// ErrOperationNotSupported is returned if an operation is not supported.
	ErrOperationNotSupported = errors.New("operation not supported")
)

// StatFS extends FS with Stat capability.
type StatFS interface {
	FS
	Stat(name string) (os.FileInfo, error)
}

// Stat returns os.FileInfo for the file with the given name if supported.
func Stat(fsys FS, name string) (os.FileInfo, error) {
	if fsys, ok := fsys.(StatFS); ok {
		return fsys.Stat(name)
	}

	file, err := fsys.Open(name)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return file.Stat()
}

// CloseFS contains the methods required for FS that need to be closed after use.
type CloseFS interface {
	FS
	Close() error
}

// Close closes the FS if supported.
func Close(fsys FS) error {
	if fsys, ok := fsys.(CloseFS); ok {
		return fsys.Close()
	}
	return ErrOperationNotSupported
}

// CreateFS extends FS with Create file.
type CreateFS interface {
	FS
	Create(name string) (File, error)
}

// Create creates a new file on the FS, including the full path.
func Create(fsys FS, name string) (File, error) {
	if fsys, ok := fsys.(CreateFS); ok {
		return fsys.Create(name)
	}
	return nil, ErrOperationNotSupported
}

// A File has the methods for file operations.
type File interface {
	Stat() (os.FileInfo, error)
	Read([]byte) (int, error)
	Close() error
}

type WriteFile interface {
	Write(b []byte) (int, error)
}

func Writer(f File, b []byte) (int, error) {
	if f, ok := f.(WriteFile); ok {
		return f.Write(b)
	}
	return 0, ErrOperationNotSupported
}

// ReadDirFS gives FS ReadDir capability.
type ReadDirFS interface {
	FS
	ReadDir(name string) ([]os.FileInfo, error)
}

// ReadDir returns the list of files or directories in the path with name.
func ReadDir(fsys FS, name string) ([]os.FileInfo, error) {
	if fsys, ok := fsys.(ReadDirFS); ok {
		return fsys.ReadDir(name)
	}
	return nil, ErrOperationNotSupported
}
