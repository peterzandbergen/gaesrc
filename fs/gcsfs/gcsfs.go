package gcsfs

import (
	"context"
	"errors"
	"os"
	"time"

	"gaesrc/fs"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

type gcsFS struct {
	sc     *storage.Client
	bucket string
}

type gcsFile struct {
	*storage.Reader
	object *storage.ObjectHandle
}

type gcsFileInfo struct {
}

var (
	ErrBadType = errors.New("bad type")
)

// New creates a new file system that gives read only access to the objects in the bucket.
func New(credsFile string, bucket string) (fs.FS, error) {
	sc, err := storage.NewClient(context.Background(), option.WithCredentialsFile(credsFile))
	if err != nil {
		return nil, err
	}
	return &gcsFS{
		sc:     sc,
		bucket: bucket,
	}, nil
}

func (r *gcsFS) Open(name string) (fs.File, error) {
	obj := r.sc.Bucket(r.bucket).Object(name)
	f, err := obj.NewReader(context.Background())
	if err != nil {
		return nil, err
	}
	return &gcsFile{
		Reader: f,
		object: obj,
	}, err
}

func (r *gcsFS) Stat(name string) (os.FileInfo, error) {
	return getFileInfo(context.Background(), r.sc.Bucket(r.bucket).Object(name))
}

func (r *gcsFS) Close() error {
	return r.sc.Close()
}

type objectInfo struct {
	*storage.ObjectAttrs
}

func (oi *objectInfo) IsDir() bool {
	return false
}

func (oi *objectInfo) ModTime() time.Time {
	return oi.ObjectAttrs.Created
}

func (oi *objectInfo) Mode() os.FileMode {
	return 0
}

func (oi *objectInfo) Name() string {
	return oi.ObjectAttrs.Name
}

func (oi *objectInfo) Size() int64 {
	return oi.ObjectAttrs.Size
}

func (oi *objectInfo) Sys() interface{} {
	return nil
}

func getFileInfo(ctx context.Context, obj *storage.ObjectHandle) (os.FileInfo, error) {
	attrs, err := obj.Attrs(ctx)
	if err != nil {
		return nil, err
	}
	return &objectInfo{attrs}, nil
}

func (f *gcsFile) Stat() (os.FileInfo, error) {
	return getFileInfo(context.Background(), f.object)
}
