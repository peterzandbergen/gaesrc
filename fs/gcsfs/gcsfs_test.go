package gcsfs

import (
	"gaesrc/fs"
	"io"
	"testing"
)

const credsFile = "/home/peza/Secrets/zandbergenmediation-nl-71be644dc54d.json"
const bucket = "staging.zandbergenmediation-nl.appspot.com"
const object = "188638d23ffd6d0fadc238d639c6717461a64b77"

func TestOpenFS(t *testing.T) {
	fsys, err := New(credsFile, bucket)
	if err != nil {
		t.Fatalf("%s", err)
	}
	defer fs.Close(fsys)

	// Open the object.
	f, err := fsys.Open(object)
	if err != nil {
		t.Fatalf("%s", err)
	}
	// Get object file info.
	stat, err := f.Stat()
	if err != nil {
		t.Fatalf("%s", err)
	}
	nw := &NullWriter{}
	n, err := io.Copy(nw, f)
	if n != stat.Size() {
		t.Errorf("expected: %d, got: %d", stat.Size(), n)
	}
	t.Logf("%#v", stat)
	defer f.Close()
}
