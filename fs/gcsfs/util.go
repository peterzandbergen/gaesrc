package gcsfs

import (
	"net/url"
	"strings"
)

type Info struct {
	Bucket string
	Object string
}

func InfoFromUrl(name string) (Info, error) {
	var res Info
	url, err := url.Parse(name)
	if err != nil {
		return res, err
	}
	parts := strings.Split(url.Path, "/")
	res.Bucket = parts[1]
	res.Object = strings.Join(parts[2:], "/")
	return res, nil
}

func BucketFromURL(url string) string {
	info, err := InfoFromUrl(url)
	if err != nil {
		return ""
	}
	return info.Bucket
}

func ObjectFromURL(url string) string {
	info, err := InfoFromUrl(url)
	if err != nil {
		return ""
	}
	return info.Object
}

type NullWriter struct{}

func (n *NullWriter) Write(p []byte) (int, error) {
	return len(p), nil
}

