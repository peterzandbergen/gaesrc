package stringfs

import (
	"bytes"
	"os"
	"time"

	"gaesrc/fs"
)

type byteFS struct{}

type byteFile struct {
	*bytes.Buffer
	name string
	createTime time.Time
}

func New() fs.FS {
	var bFS byteFS
	return bFS
}

func (bfs byteFS) Open(name string) (fs.File, error) {
	return &byteFile{
		Buffer: bytes.NewBufferString(name),
		name: name,
		createTime: time.Now(),
	}, nil
}

func (bf *byteFile) Close() error {
	return nil
}

type objectInfo struct {
	modTime time.Time
	name string
	size int64
}

func (oi *objectInfo) IsDir() bool {
	return false
}

func (oi *objectInfo) ModTime() time.Time {
	return oi.modTime
}

func (oi *objectInfo) Mode() os.FileMode {
	return 0
}

func (oi *objectInfo) Name() string {
	return oi.name
}

func (oi *objectInfo) Size() int64 {
	return oi.size
}

func (oi *objectInfo) Sys() interface{} {
	return nil
}

func (bf *byteFile) Stat() (os.FileInfo, error) {
	return &objectInfo{
		modTime: bf.createTime,
		name: bf.name,
		size: int64(bf.Buffer.Cap()),
	}, nil
}