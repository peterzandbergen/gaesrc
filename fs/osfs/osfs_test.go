package osfs

import (
	"gaesrc/fs"
	"io"
	"os"
	"path/filepath"
	"testing"
)

func TestCreateFile(t *testing.T) {
	const filename = "testfile.test"
	dir := t.TempDir()
	fsys, err := New(dir)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	defer fs.Close(fsys)
	t.Logf("%#v", fsys)
	f, err := fs.Create(fsys, filename)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	if err := f.Close(); err != nil {
		t.Fatalf("error closing f: %s", err)
	}
	t.Logf("%#v", f)
	// Test if file exists.
	s, err := os.Stat(filepath.Join(dir, filename))
	if err != nil {
		t.Fatalf("file create failed: %s", err)
	}
	// Test if file is a file.
	if s.IsDir() {
		t.Fatalf("file is a directory")
	}
}

func TestCopy(t *testing.T) {
	fsys, err := New(t.TempDir())
	if err != nil {
		t.Fatalf("%s", err)
	}
	f, err := fs.Create(fsys, "testfile")
	if err != nil {
		t.Fatalf("%s", err)
	}
	defer f.Close()
	// Get writer interface.
	w, ok := f.(io.Writer)
	if !ok {
		t.Fatalf("error getting io.Writer")
	}
	_ = w
}
