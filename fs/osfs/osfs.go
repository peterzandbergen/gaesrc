package osfs

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"gaesrc/fs"
)

var (
	// ErrRootNotDir is returned when the root is not and existing directory.
	ErrRootNotDir = errors.New("root is not a directory")
	// ErrRelativePath is returned when the path is not relative.
	ErrRelativePath = errors.New("path is not relative")
)

// osFS is the FS for os and represents a directory on the os.
type osFS string

// osFile represents a file on the osFS.
type osFile struct {
	*os.File
}

// New returns a new os fs under the given root.
// The root must exist and must be a directory.
func New(root string) (fs.FS, error) {
	root, err := filepath.Abs(root)
	if err != nil {
		return nil, err
	}
	s, err := os.Stat(root)
	if err != nil {
		return nil, err
	}
	if !s.IsDir() {
		return nil, ErrRootNotDir
	}
	return osFS(root), nil
}

// fullPath returns the full path for the given name.
func (r osFS) fullPath(name string) string {
	res := filepath.Join(string(r), name)
	return res
}

// Open returns a file for reading.
// This implements the fs.FS interface.
func (r osFS) Open(name string) (fs.File, error) {
	name = filepath.Clean(name)
	if filepath.IsAbs(name) {
		return nil, ErrRelativePath
	}
	f, err := os.Open(r.fullPath(name))
	if err != nil {
		return nil, err
	}
	return &osFile{f}, nil
}

func createAll(name string) (*os.File, error) {
	// Create dir if it not exists.
	dir := filepath.Dir(name)
	if _, err := os.Stat(dir); err != nil {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return nil, err
		}
	}
	// Create the file.
	f, err := os.Create(name)
	if err != nil {
		return nil, err
	}
	return f, nil
}

// Create creates a file for writing. Existing files are truncated.
// This implements the fs.CreateFS interface.
func (r osFS) Create(name string) (fs.File, error) {
	f, err := createAll(r.fullPath(name))
	if err != nil {
		return nil, err
	}
	return &osFile{f}, nil
}

// ReadDir returns the FileInfo's for the given directory.
// This implements the fs.ReadDirFS interface.
func (r osFS) ReadDir(name string) ([]os.FileInfo, error) {
	return ioutil.ReadDir(r.fullPath(name))
}
