package gaesrc

import (
	"context"
	"gaesrc/apps"

	"google.golang.org/api/option"

	"cloud.google.com/go/storage"
)

type GCP struct {
	CredsFile string
	AppEngine *apps.AppEngineClient
	Storage   *storage.Client
}

func NewGCP(credsFile string) (*GCP, error) {
	ae, err := apps.New(credsFile)
	if err != nil {
		return nil, err
	}
	s, err := storage.NewClient(context.Background(), option.WithCredentialsFile(credsFile))
	if err != nil {
		return nil, err
	}
	return &GCP{
		AppEngine: ae,
		Storage:   s,
	}, nil
}

// func (g *GCP) Projects() ()
