package transferfs

import (
	"errors"
	"gaesrc/fs"
	"io"
)

// temp error for testing.
var errNotImplementedYet = errors.New("not implemented yet")

var (
	ErrLengthsNotMatching = errors.New("length of src and dst do not match")
	ErrTypeError          = errors.New("bad type")
)

func Copy(dstFS fs.FS, dst string, srcFS fs.FS, src string) error {
	// Open the source.
	srcf, err := srcFS.Open(src)
	if err != nil {
		return err
	}
	defer srcf.Close()
	dstf, err := fs.Create(dstFS, dst)
	if err != nil {
		return err
	}
	// We need a writer interface.
	w, ok := dstf.(io.Writer)
	if !ok {
		return ErrTypeError
	}
	defer dstf.Close()
	_, err = io.Copy(w, srcf)
	return err
}

func CopyN(dstFS fs.FS, dst []string, srcFS fs.FS, src []string) error {
	if len(src) != len(dst) {
		return ErrLengthsNotMatching
	}
	for i := 0; i < len(dst); i++ {
		err := Copy(dstFS, dst[i], srcFS, src[i])
		if err != nil {
			return err
		}
	}
	return nil
}
