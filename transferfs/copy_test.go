package transferfs

import (
	"gaesrc/fs"
	"gaesrc/fs/osfs"
	"gaesrc/fs/stringfs"
	"os"
	"path/filepath"

	"testing"
)

func TestCopy(t *testing.T) {
	srcFS := stringfs.New()
	defer fs.Close(srcFS)

	dstdir := t.TempDir()
	dstname := "dsttestfile"
	dsttestpath := filepath.Join(dstdir, dstname)
	dstFS, err := osfs.New(dstdir)
	if err != nil {
		t.Fatalf("%s", err)
	}
	defer fs.Close(dstFS)
	err = Copy(dstFS, dstname, srcFS, "srcfile")
	if err != nil {
		t.Fatalf("%s", err)
	}
	// Test if the file exists.
	if _, err := os.Stat(dsttestpath); err != nil {
		t.Fatalf("file not created")
	}
	t.Logf("file created: %s", dsttestpath)
}

func TestCopyN(t *testing.T) {
	srcFS := stringfs.New()
	defer fs.Close(srcFS)

	dstdir := t.TempDir()
	dstnames := []string{
		"dsttestfile1", 
		"dsttestfile2", 
	}
	srcnames := []string{
		"srctestfile1", 
		"srctestfile2", 
	}
		
	dstFS, err := osfs.New(dstdir)
	if err != nil {
		t.Fatalf("%s", err)
	}
	defer fs.Close(dstFS)
	err = CopyN(dstFS, dstnames, srcFS, srcnames)
	if err != nil {
		t.Fatalf("%s", err)
	}
}