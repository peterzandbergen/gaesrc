package main

import (
	"context"
	"fmt"

	"github.com/spf13/pflag"

	"cloud.google.com/go/storage"
)

type config struct {
	targetDir   string
	rest        []string
	projectName string
	credsFile   string
}

func copyFile(src, dst string) error {
	ctx := context.Background()
	c, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}
	_ = c

	return nil
}

func parseConfig(args []string) *config {
	if len(args) == 0 {
		panic("args needs at least one element (test error)")
	}
	cfg := &config{}
	fs := pflag.NewFlagSet(args[0], pflag.ExitOnError)
	// Declare the flags.
	fs.StringVarP(&cfg.targetDir, "targetdir", "d", ".", "target directory for the sources")
	fs.Parse(args[1:])
	cfg.rest = fs.Args()
	return cfg
}

func execute() {
	
}

func main() {
	fmt.Println("hallo")
}
